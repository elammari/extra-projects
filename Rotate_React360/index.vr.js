// Some explanation here https://medium.com/p/a928ca90f489

import React from 'react';
import { AppRegistry, asset, Pano, Text, View, VrHeadModel } from 'react-vr';
const RCTDeviceEventEmitter = require('RCTDeviceEventEmitter');

class Rotate extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			aov: VrHeadModel.rotation(),
			colorX: 255,
			colorY: 0
		}

		// Log available listeners
		console.log('RCT Signals', RCTDeviceEventEmitter.addListener().subscriber._subscriptionsForType)

		// You may use 'onReceivedHeadMatrix' or others based on your needs
		// but 'onReceivedHeadMatrix' triggers infinitely in browsers.
		RCTDeviceEventEmitter.addListener('onReceivedInputEvent', e => {

			// Log what event is happening
			// console.log('Event type', e)

			this.setState({
				aov: VrHeadModel.rotation()
			}, () => this.generateColors())

		});
	}

	generateColors = () => {
		// Valid color or pass zero to rgb
		const newColorX = this.state.aov[0] < 90 ? 255-Math.abs(this.state.aov[0]*2.8) : 0
		const newColorY = Math.abs(this.state.aov[1]) < 90 ? Math.abs(this.state.aov[1]*2.8) : 0

		const newColorXRounded = Number(newColorX).toFixed(0)
		const newColorYRounded = Number(newColorY).toFixed(0)

		this.setState({
			colorX: newColorXRounded,
			colorY: newColorYRounded,
		})
	}
  render() {

	// For the sake of the example we used rotation
	// But horizontalFov() and verticalFov() are also available.
	const xRotRounded = Number((this.state.aov[0]).toFixed(0))
	const yRotRounded = Number((this.state.aov[1]).toFixed(0))

	console.log('xRotRounded: ' + xRotRounded + '°', 'yRotRounded: ' + yRotRounded + '°')

	return (
    
	  <View>
		<Pano source={asset('chess-world.jpg')}/>
			<Text
			style={{
				backgroundColor: `rgb(${this.state.colorX}, ${this.state.colorY}, 55)`,
				fontSize: 0.8,
				layoutOrigin: [0.5, 0.5],
				paddingLeft: 0.2,
				paddingRight: 0.2,
				textAlign: 'center',
				textAlignVertical: 'center',
				transform: [{translate: [0, 0, -3]}, {scale: 0.5}],
			}}>
			x: {xRotRounded}° y: {yRotRounded}°
			</Text>
	  </View>
	);
  }
};

AppRegistry.registerComponent('Rotate', () => Rotate);
